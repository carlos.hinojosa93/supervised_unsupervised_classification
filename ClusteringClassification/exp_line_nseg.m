clear all
close all
clc

load('simu_Nseg_old');

x = 10:10:4000;
x = x(1:5:400);
x = x';
y = OAbyNseg';
y = y(1:5:400);

f = fit(x,y,'exp2');

y1 = f.a * exp(f.b*x) + f.c*exp(f.d*x);
y2 = ones(size(y1))*0.9692;

data = [x 100*y 100*y1 100*y2];
save OAvsNSEG.dat data -ascii;

%% Plot
figure
plot(x,y,'-s','Color',[57/255,106/255,177/255],'LineWidth',1.5);
hold on
refC = plot(f);
refC.Color = [204/255,37/255,41/255];
refC.LineWidth = 1.5;
hold on
hline = refline([0,0.9692]);
hline.Color = [114/255,170/255,64/255];
hline.LineWidth = 1.5;
xticks(x(1:7:80));
xlabel('N_{seg}');
ylabel('Overall Accuracy (OA)');
title('Accuracy of classification varying the number of segments N_{seg}');

legend('Proposed Method','Fitted Curve','Only Spectral Classification');