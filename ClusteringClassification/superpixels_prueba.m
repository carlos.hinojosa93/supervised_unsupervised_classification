clear all
clc
close all

[Nh, ~, Lh]   = size(data_hypt);
[Nm, ~, Lm]   = size(data_mult);
Fh            = reshape(data_hypt, Nh*Nh, Lh);
Fm            = reshape(data_mult, Nm*Nm, Lm);
Fori           = reshape(data_ori,Nm*Nm,Lh);

Nseg = 500;

% segmentation procedure
[segm, Nseg]  = superpixels(data_mult, Nseg,...
                        'Compactness', 20, 'Method',...
                               'slic', 'NumIterations', 100);

% crete segmentation matrix
val1          = [];
val2          = [];
idxx          = [];
idxy          = [];
for nseg = 1:Nseg
    idx       = find(segm(:) == nseg);
    idxx      = [idxx; idx];
    idxy      = [idxy; ones(length(idx), 1)*nseg];
    val1      = [val1; ones(length(idx), 1)];
    val2      = [val2; 1./length(idx)];
end  
U             = sparse(idxy, idxx, val1, Nseg, Nm*Nm);  
W             = sparse(1:Nseg, 1:Nseg, val2, Nseg, Nseg);
Uw            = W*U;

% measurements 
Dm            = kron(speye(Nh), sparse(ones(1, fctr)));
Dm            = kron(Dm, Dm)./sqrt(fctr);
Yh            = awgn(Fh, 50, 'measured');
Yh            = Yh./max(Yh(:));
lmb_max       = norm(Yh, 'inf');
Fh_hat        = low_rank_admm(Dm*Uw', Yh, tau*lmb_max,...
                                            [Nseg Lh], []);
F_est         = U'*Fh_hat;

