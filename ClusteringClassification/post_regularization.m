function [Ir] = post_regularization(Io, T1, itmax, error_threshold, window)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if (nargin == 4)
    window = '8-neighbor-Chamfer';
end

if strcmp(window,'8-neighbor-Chamfer')
    [N1,N2] = size(Io);
    Ir = Io;
    I_old = Io;
    I_new = padarray(Io,[1 1],'symmetric');
    error_image = 1;
    iteration = 0;
    while ((error_image > error_threshold)&&(iteration < itmax))
        for j = 2 : N2 + 1
            for i = 2 : N1 + 1
                patch = I_new(i-1:i+1,j-1:j+1);
                pv = patch(:);
                pv = [pv(1:4); pv(6:9)];
                uv = unique(pv);
                nv = hist(pv,length(uv));
                [mv,iv] = max(nv);
                if mv > T1
                    Ir(i-1,j-1) = uv(iv);
                end
            end
        end
%        Ir = I_new(2:N1+1,2:N2+1);
        error_image = sum((Ir(:) ~= I_old(:)))/(N1*N2);
        I_old = Ir;
        I_new = padarray(Ir,[1 1],'symmetric');
        iteration = iteration + 1;
    end
end


end

