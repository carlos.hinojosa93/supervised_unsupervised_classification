%%  Spectral Image Classification-Clustering from Compressive Measurements and Data Fusion
%
%   Routine: ClusteringClassification
%
%   Authors:
%   Juan Marcos Ramirez,
%   Carlos Hinojosa
%   Universidad Industrial de Santander, Bucaramanga, Colombia
%   email: juanmarcos26@gmail.com
%
%   Date: May, 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
close all;

addpath(genpath('ReconstructionFunctions'));
addpath(genpath('HSMSFusionToolbox'));

% Loading data
load('../Data/Salinas_corrected.mat');
Io = salinas_corrected(1:217,1:217,11:202);
clear salinas_corrected;

load('../Data/Salinas_gt.mat');
ground_truth = salinas_gt(1:217,1:1:217);
clear salinas_gt;

L   = size(Io,3);
N1  = size(Io,1);
N2  = size(Io,2);
%dibujar RGB -> imagesc(mat2gray(Io(:,:,[66,53,33])))

% Ground truth correction
unq = unique(ground_truth);

for k = 1:length(unq) - 1
    for i = 1:size(ground_truth,1)
        for j =1:size(ground_truth,2)
            if (ground_truth(i,j) == unq(k+1))
                ground_truth(i,j) = k;
            end
        end
    end
end
band_set = [60 80 120];

for kk =1:3
    t1 = Io(:,:,band_set(kk));
    temp_show(:,:,kk) = (t1 - min(t1(:)))/(max(t1(:)) - min(t1(:)));
end

imshow(temp_show,[],'Border','tight');
text(178, 15, '(a)', 'Color', [0 0 0], 'Fontsize', 22,'FontWeight','Bold','FontName','Times');
print('-depsc', 'hri_salinas');
 
imshow(label2rgb(ground_truth),'Border','tight');
text(178, 15, '(b)', 'Color', [0 0 0], 'Fontsize', 22,'FontWeight','Bold','FontName','Times');
print('-depsc', 'gt_salinas');

% % Simulation parameters
% SNR                 = 25;   %additive noise in dB
% trials              = 51;
% training_rate       = 0.10;
% compression_ratio   = [0.125 0.25 0.50 0.75 1.00];
% Nseg                = 16;
% 
% % Building the hyperspectral image (spatial downsampling)
% p       = 2;
% window  = 5;
% sigma   = 1.00;
% I_HS    = spatial_blurring(Io, p, window, sigma);
% [N1_HS, N2_HS,L_HS] = size(I_HS);
% 
% 
% % Building the multispectral image (spectral downsampling)
% q = 4;
% I_MS = spectral_blurring(Io, q);
% [N1_MS, N2_MS,L_MS] = size(I_MS);
% 
% for jj = 1:length(compression_ratio)
%     for ii = 1:trials
%         tic;
%         disp(['Compression ratio: ' num2str(compression_ratio(jj)*100) 'Iteration: ' num2str(ii)]);
%         
%         num_hs_filters      = floor(L_HS * compression_ratio(jj));
%         shots_HS            = floor(L_HS * compression_ratio(jj));
%         
%         num_ms_filters      = floor(L_MS * compression_ratio(jj));
%         shots_MS            = floor(L_MS * compression_ratio(jj));
% 
% 
%         [training_indexes, test_indexes] = classification_indexes(ground_truth, training_rate);
%         T_classes = ground_truth(training_indexes);
%         t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
%         
%         %% Clean Measurements
%         % 3D-CASSI shots
%         [shot_patt_HS, ~, ~, cca_HS, filter_pattHS] = patterned_shots(I_HS, compression_ratio(jj));
%         [shot_patt_MS, ~, ~, cca_MS, filter_pattMS] = patterned_shots(I_MS, compression_ratio(jj));
%         
%         tStart1 = tic;
%         % Feature extraction from HS compressive measurements
%         features_hs = feature_extraction_Patterned(shot_patt_HS, size(I_HS),...
%             shots_HS, num_hs_filters, filter_pattHS);
%         features_hs_hd = interpolate_hs_features(features_hs,[N1_HS N2_HS], p, 'nearest');
%         
%         features            = features_hs_hd;
%         features_train      = features(:,training_indexes);
%         features_test       = features(:,test_indexes);
%         
%         % Feature extraction from MS compressive measurements
%         features_ms = feature_extraction_Patterned(shot_patt_MS, size(I_MS),...
%             shots_MS, num_ms_filters, filter_pattMS);
%         disp("Nseg actual: "+Nseg);
%         
%         if size(features_ms,1)>3
%             features_ms = hyperPct(features_ms,3);
%         end
%         
%         data_mult = reshape(features_ms',[N1_MS N2_MS 3]);
%         
%         [segm, Nseg1]  = superpixels(data_mult, Nseg,...
%             'Compactness', 20, 'Method',...
%             'slic', 'NumIterations', 100);
%         features_ms2 = zeros(size(features_ms));
%         for nseg = 1:Nseg1
%             idx       = find(segm(:) == nseg);
%             meanSig = mean(features_ms(:,idx),2);
%             features_ms2(:,idx)=repmat(meanSig,1,length(idx));
%         end
%         
%         % Stacking
%         features_s          = [features; features_ms2];
%         features_s_train    = features_s(:,training_indexes);
%         features_s_test     = features_s(:,test_indexes);
%         
%         % Classification stage
%         disp('Predicting classes using the SVM-PLY approach...');
%         Mdl1         = fitcecoc(features_s_train',T_classes,'Learners',t);
%         class_hat1   = predict(Mdl1, features_s_test');
%         tElapsed1(ii,jj) = toc(tStart1);
%         
%         %% Noisy Measurements
%         
%         %==> add noise to measurements
%         shot_patt_HS_noisy = awgn(reshape(shot_patt_HS,[N1_HS*N2_HS,shots_HS])',...
%             SNR,'measured');
%         shot_patt_HS_noisy = reshape(shot_patt_HS_noisy',[N1_HS,N2_HS,shots_HS]);
%         
%         %==> add noise to measurements
%         shot_patt_MS_noisy = awgn(reshape(shot_patt_MS,[N1_MS*N2_MS,shots_MS])',...
%             SNR,'measured');
%         shot_patt_MS_noisy = reshape(shot_patt_MS_noisy',[N1_MS,N2_MS,shots_MS]);
%         
%         tStart2 = tic;
%         % Feature extraction from HS compressive measurements
%         features_hs = feature_extraction_Patterned(shot_patt_HS_noisy, size(I_HS),...
%             shots_HS, num_hs_filters, filter_pattHS);
%         features_hs_hd = interpolate_hs_features(features_hs,[N1_HS N2_HS], p, 'nearest');
%         
%         features            = features_hs_hd;
%         features_train      = features(:,training_indexes);
%         features_test       = features(:,test_indexes);
%         
%         % Feature extraction from MS compressive measurements
%         features_ms = feature_extraction_Patterned(shot_patt_MS_noisy, size(I_MS),...
%             shots_MS, num_ms_filters, filter_pattMS);
%         disp("Nseg actual: "+Nseg);
%         
%         if size(features_ms,1)>3
%             features_ms = hyperPct(features_ms,3);
%         end
%         
%         data_mult = reshape(features_ms',[N1_MS N2_MS 3]);
%         
%         [segm, Nseg1]  = superpixels(data_mult, Nseg,...
%             'Compactness', 20, 'Method',...
%             'slic', 'NumIterations', 100);
%         features_ms2 = zeros(size(features_ms));
%         for nseg = 1:Nseg1
%             idx       = find(segm(:) == nseg);
%             meanSig = mean(features_ms(:,idx),2);
%             features_ms2(:,idx)=repmat(meanSig,1,length(idx));
%         end
%         
%         % Stacking
%         features_s          = [features; features_ms2];
%         features_s_train    = features_s(:,training_indexes);
%         features_s_test     = features_s(:,test_indexes);
%         
%         % Classification stage
%         disp('Predicting classes using the SVM-PLY approach...');
%         Mdl2         = fitcecoc(features_s_train',T_classes,'Learners',t);
%         class_hat2   = predict(Mdl2, features_s_test');
%         tElapsed2(ii,jj) = toc(tStart2);
%         
%         %% Raw Image classification
%         tStart3 = tic;
%         
%         features_s          = reshape(Io,[N1*N2 L])';
%         features_s_train    = features_s(:,training_indexes);
%         features_s_test     = features_s(:,test_indexes);
%         
%         % Classification stage
%         disp('Predicting classes using the SVM-PLY approach...');
%         Mdl3         = fitcecoc(features_s_train',T_classes,'Learners',t);
%         class_hat3   = predict(Mdl3, features_s_test');
%         tElapsed3(ii,jj) = toc(tStart3);
%         
%         %% Reconstruction + Fusion
%         % Recovering hyperspectral image
%         tStart4 = tic;
%         RH = cassi_reconstruction_admm(shot_patt_HS, cca_HS);
%         RM = cassi_reconstruction_admm(shot_patt_MS, cca_MS);
%         Ir = CNMF_fusion(RH, RM);
%         
%         features_s          = reshape(Ir,[N1*N2 L])';
%         features_s_train    = features_s(:,training_indexes);
%         features_s_test     = features_s(:,test_indexes);
%         
%         % Classification stage
%         disp('Predicting classes using the SVM-PLY approach...');
%         Mdl4         = fitcecoc(features_s_train',T_classes,'Learners',t);
%         class_hat4   = predict(Mdl4, features_s_test');
%         tElapsed4(ii,jj) = toc(tStart4);
%         
%         %% Saving classification results
%         % Building the classification map
%         training_set_image = zeros(size(ground_truth,1), size(ground_truth,2));
%         training_set_image(training_indexes) = ground_truth(training_indexes);
%         
%         SVM_classification_clean = class_map_image(ground_truth, class_hat1, training_indexes, test_indexes);
%         SVM_classification_noisy = class_map_image(ground_truth, class_hat2, training_indexes, test_indexes);
%         SVM_classification_image = class_map_image(ground_truth, class_hat3, training_indexes, test_indexes);
%         SVM_classification_fused = class_map_image(ground_truth, class_hat4, training_indexes, test_indexes);
%         
%         [OA1(ii,jj), AA1(ii,jj), kappa1(ii,jj)] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_clean(test_indexes)));
%         [OA2(ii,jj), AA2(ii,jj), kappa2(ii,jj)] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_noisy(test_indexes)));
%         [OA3(ii,jj), AA3(ii,jj), kappa3(ii,jj)] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_image(test_indexes)));
%         [OA4(ii,jj), AA4(ii,jj), kappa4(ii,jj)] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_fused(test_indexes)));
%         
%         toc;
%     end
% end
% 
% %% Display results
% % Overall accuracy
% mOA(1,:) = mean(OA1(2:end,:));
% mOA(2,:) = mean(OA2(2:end,:));
% mOA(3,:) = mean(OA3(2:end,:));
% mOA(4,:) = mean(OA4(2:end,:));
% 
% % Average accuracy
% mAA(1,:) = mean(AA1(2:end,:));
% mAA(2,:) = mean(AA2(2:end,:));
% mAA(3,:) = mean(AA3(2:end,:));
% mAA(4,:) = mean(AA4(2:end,:));
% 
% % Cohen's kappa statistic
% mkappa(1,:) = mean(kappa1(2:end,:));
% mkappa(2,:) = mean(kappa2(2:end,:));
% mkappa(3,:) = mean(kappa3(2:end,:));
% mkappa(4,:) = mean(kappa4(2:end,:));
% 
% % Computation time
% mtElapsed(1,:) = mean(tElapsed1(2:end,:));
% mtElapsed(2,:) = mean(tElapsed2(2:end,:));
% mtElapsed(3,:) = mean(tElapsed3(2:end,:));
% mtElapsed(4,:) = mean(tElapsed4(2:end,:));
% 
% 
% subplot(221);
% plot(compression_ratio, mOA*100,'LineWidth',2);
% legend('Compressive','Compressive-noisy','Raw image','Rec-CNMF');
% axis('tight');
% ylabel('OA (%)');
% xlabel('Compression ratio (%)');
% 
% subplot(222);
% plot(compression_ratio, mAA*100,'LineWidth',2);
% legend('Compressive','Compressive-noisy','Raw image','Rec-CNMF');
% axis('tight');
% ylabel('AA (%)');
% xlabel('Compression ratio (%)');
% 
% subplot(223);
% plot(compression_ratio, mkappa,'LineWidth',2);
% legend('Compressive','Compressive-noisy','Raw image','Rec-CNMF');
% axis('tight');
% ylabel('\kappa');
% xlabel('Compression ratio (%)');
% 
% subplot(224);
% semilogy(compression_ratio, mtElapsed,'LineWidth',2);
% legend('Compressive','Compressive-noisy','Raw image','Rec-CNMF');
% axis('tight');
% ylabel('Computation time (s)');
% xlabel('Compression ratio (%)');
% 
% data1 = [100*compression_ratio' 100*mOA'];
% save OAvsCompression.dat data1 -ascii;
% save OAvsCompression.mat data1;
% data2 = [100*compression_ratio' 100*mAA'];
% save AAvsCompression.dat data2 -ascii;
% save AAvsCompression.mat data2;
% data3 = [100*compression_ratio' mkappa'];
% save KAPPAvsCompression.dat data3 -ascii;
% save KAPPAvsCompression.mat data3;
% data4 = [100*compression_ratio' mtElapsed'];
% save TimevsCompression.dat data4 -ascii;
% save TimevsCompression.mat data4;