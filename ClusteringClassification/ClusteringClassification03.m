%%  Spectral Image Classification-Clustering from Compressive Measurements and Data Fusion
%
%   Routine: ClusteringClassification
%
%   Author:
%   Juan Marcos Ramirez,
%   Universidad Industrial de Santander, Bucaramanga, Colombia
%   email: juanmarcos26@gmail.com
%
%   Date: May, 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
close all;

load('pavia3.mat');
Io = paviaU(:,:,1:96);
ground_truth = paviaU_gt;

clear paviaU;
clear paviaU_gt;

L   = size(Io,3);
N1  = size(Io,1);
N2  = size(Io,2);

% Ground truth correction
for i = 1:N1
    for j = 1:N2
        if (ground_truth(i,j) > 2)
            ground_truth(i,j) = ground_truth(i,j) - 1;
        end
    end
end

training_rate = 0.10;
[training_indexes, test_indexes] = classification_indexes(ground_truth, training_rate);
T_classes = ground_truth(training_indexes);

% Building the hyperspectral image (spatial downsampling)
p       = 2;
window  = 5;
sigma   = 1.00;
I_HS    = spatial_blurring(Io, p, window, sigma);

q = 8;
I_MS = spectral_blurring(Io, q);

% SSCSI shots
compression_ratio   = 0.25;
[N1_HS, N2_HS,L_HS] = size(I_HS);
num_hs_filters      = floor(L_HS * compression_ratio);
shots_HS            = floor(L_HS * compression_ratio);
[shot_patt_HS, ~, ~, ~, filter_pattHS] = patterned_shots(I_HS, compression_ratio);

[N1_MS, N2_MS,L_MS] = size(I_MS);
num_ms_filters      = floor(L_MS * compression_ratio);
shots_MS            = floor(L_MS * compression_ratio);
[shot_patt_MS, ~, ~, ~, filter_pattMS] = patterned_shots(I_MS, compression_ratio);

% Feature extraction from HS compressive measurements
features_hs = feature_extraction_Patterned(shot_patt_HS, size(I_HS),...
    shots_HS, num_hs_filters, filter_pattHS);
clear shot_patt_HS filter_pattHS;

features_ms = feature_extraction_Patterned(shot_patt_MS, size(I_MS),...
    shots_MS, num_ms_filters, filter_pattMS);
clear shot_patt_HS filter_pattHS;

total_hs_indexes = 1:N1 * N2;
total_indexes    = floor((total_hs_indexes-1)/p) - (N1/p) * floor((total_hs_indexes-1)/(N1)) + (N1/p) * floor((total_hs_indexes-1)/(p*N1));
features_hs_hd   = features_hs(:,total_indexes+1);

features = features_ms;
features_train     = features(:,training_indexes);
features_test      = features(:,test_indexes);

% Clustering stage
cluster_data = kmeans(features_hs',max(ground_truth(:))+1,'Distance','cityblock','MaxIter',500);
cluster_image = cluster_data(total_indexes + 1);
cluster_image = reshape(cluster_image,[N1 N2]);

% Classification stage
disp('Predicting classes using the SVM-PLY approach...');
t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
Mdl         = fitcecoc(features_train',T_classes,'Learners',t);
class_hat   = predict(Mdl, features_test');

% Building the classification map
training_set_image = zeros(size(ground_truth,1), size(ground_truth,2));
training_set_image(training_indexes) = ground_truth(training_indexes);
SVM_classification_image = class_map_image(ground_truth, class_hat, training_indexes, test_indexes);

% Majority voting image
majority_image = majority_voting(cluster_image, SVM_classification_image);

% Computing accuracy
[OA, AA, kappa] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_image(test_indexes)));

% Display results
subplot(231)
imshow(mean(I_HS,3),[]);
title('HS image');
subplot(232)
imshow(mean(I_MS,3),[]);
title('MS image');
subplot(233)
imshow(label2rgb(ground_truth));
title('Ground truth');
subplot(234)
imshow(label2rgb(cluster_image));
title('Clustering with HS');
subplot(235)
imshow(label2rgb(SVM_classification_image));
title('Classification with MS');
xlabel(['OA = ' num2str(100*OA,4) ' %']);
subplot(236)
imshow(label2rgb(majority_image));
title('Majority voting image');

%% Including background as a class

training_rate = 0.20;
ground_truth = ground_truth +1;
[training_indexes, test_indexes] = classification_indexes(ground_truth, training_rate);
T_classes = ground_truth(training_indexes);

features_train     = features(:,training_indexes);
features_test      = features(:,test_indexes);

% Classification stage
disp('Predicting classes using the SVM-PLY approach...');
t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
Mdl         = fitcecoc(features_train',T_classes,'Learners',t);
class_hat   = predict(Mdl, features_test');

% Building the classification map
training_set_image = zeros(size(ground_truth,1), size(ground_truth,2));
training_set_image(training_indexes) = ground_truth(training_indexes);
SVM_classification_image = class_map_image(ground_truth, class_hat, training_indexes, test_indexes);

% Majority voting image
majority_image = majority_voting(cluster_image, SVM_classification_image);

% Computing accuracy
[OA, AA, kappa] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_image(test_indexes)));

figure;
subplot(221)
imshow(label2rgb(ground_truth));
title('Ground truth');
subplot(222)
imshow(label2rgb(cluster_image));
title('Clustering with HS');
subplot(223)
imshow(label2rgb(SVM_classification_image));
title('Classification with MS');
xlabel(['OA = ' num2str(100*OA,4) ' %']);
subplot(224)
imshow(label2rgb(majority_image));
title('Majority voting image');