%%  Spectral Image Classification-Clustering from Compressive Measurements and Data Fusion
%
%   Routine: ClusteringClassification
%
%   Author:
%   Juan Marcos Ramirez,
%   Universidad Industrial de Santander, Bucaramanga, Colombia
%   email: juanmarcos26@gmail.com
%
%   Date: May, 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
close all;

addpath(genpath('ReconstructionFunctions'))

% Loading data
load('../Data/PaviaU.mat');
Io = paviaU(end-255:end,1:256,1:1:96);
clear paviaU;

load('../Data/PaviaU_gt.mat');
ground_truth = paviaU_gt(end-255:end,1:256);
clear paviaU_gt;

L   = size(Io,3);
N1  = size(Io,1);
N2  = size(Io,2);
%dibujar RGB -> imagesc(mat2gray(Io(:,:,[66,53,33])))
% Ground truth correction
for i = 1:N1
    for j = 1:N2
        if (ground_truth(i,j) > 5)
            ground_truth(i,j) = ground_truth(i,j) - 1;
        end
    end
end

noise = 25; %ruido añadido a las medidas CSI

nexp = 10; %numero de experimentos
% segIni = 5;
% segStep = 5;
% segEnd = 4000;
segIni = 10;
segStep = 10;
segEnd = 10;
OAbyNseg = zeros(1,length(segIni:segStep:segEnd));
AAbyNseg = zeros(1,length(segIni:segStep:segEnd));
kappabyNseg = zeros(1,length(segIni:segStep:segEnd));
classCbyNseg = zeros(length(unique(ground_truth))-1,length(segIni:segStep:segEnd));
timebyNseg = zeros(1,length(segIni:segStep:segEnd));
nit = 0;
for Nseg = segIni:segStep:segEnd
    nit = nit +1;
    valsOA = zeros(1,nexp);
    valsAA = zeros(1,nexp);
    valsKappa = zeros(1,nexp);
    valsClassC = zeros(length(unique(ground_truth))-1,nexp);
    valsTime = zeros(1,nexp);
    bestOA = 0;
    bestNseg = 0;
    for ne=1:nexp
        tic;
        training_rate = 0.10;
        [training_indexes, test_indexes] = classification_indexes(ground_truth, training_rate);
        T_classes = ground_truth(training_indexes);
        
        % Building the hyperspectral image (spatial downsampling)
        p       = 2;
        window  = 5;
        sigma   = 1.00;
        I_HS    = spatial_blurring(Io, p, window, sigma);
        
        q = 8;
        I_MS = spectral_blurring(Io, q);
        
        
        % 3D-CASSI shots
        compression_ratio   = 0.25; %==>1 para full image, 0.25 compressive y reconstruct
        [N1_HS, N2_HS,L_HS] = size(I_HS);
        num_hs_filters      = floor(L_HS * compression_ratio);
        shots_HS            = floor(L_HS * compression_ratio);
        [shot_patt_HS, ~, ~, cca_HS, filter_pattHS] = patterned_shots(I_HS, compression_ratio);
        
        
        
%         %==> add noise to measurements
%         shot_patt_HS = awgn(reshape(shot_patt_HS,[N1_HS*N2_HS,shots_HS])',...
%             noise,'measured');
%         shot_patt_HS = reshape(shot_patt_HS',[N1_HS,N2_HS,shots_HS]);
        
        
        
        [N1_MS, N2_MS,L_MS] = size(I_MS);
        num_ms_filters      = floor(L_MS * compression_ratio);
        shots_MS            = floor(L_MS * compression_ratio);
        [shot_patt_MS, ~, ~, cca_MS, filter_pattMS] = patterned_shots(I_MS, compression_ratio);
        
        
        
%         %==> add noise to measurements
%         shot_patt_MS = awgn(reshape(shot_patt_MS,[N1_MS*N2_MS,shots_MS])',...
%             noise,'measured');
%         shot_patt_MS = reshape(shot_patt_MS',[N1_MS,N2_MS,shots_MS]);
        
        
        
        % Feature extraction from HS compressive measurements
        features_hs = feature_extraction_Patterned(shot_patt_HS, size(I_HS),...
            shots_HS, num_hs_filters, filter_pattHS);
        
%         %==> descomentar para reconstruccion
%         % Recovering hyperspectral image
%         RH = cassi_reconstruction_admm(shot_patt_HS, cca_HS);
%         features_hs = reshape(RH,[N1_HS*N2_HS,L_HS])';
        
        
        
        features_hs_hd = interpolate_hs_features(features_hs,[N1_HS N2_HS], p, 'nearest');
        
        
        
        %         clear shot_patt_HS filter_pattHS;
        
        features_ms = feature_extraction_Patterned(shot_patt_MS, size(I_MS),...
            shots_MS, num_ms_filters, filter_pattMS);
        %         clear shot_patt_HS filter_pattHS;
        
%         %===> descomentar para reconstruccion
%         % Recovering multispectral image
%         RM = cassi_reconstruction_admm(shot_patt_MS, cca_MS);
%         features_ms = reshape(RM,[N1_MS*N2_MS,L_MS])';
        
        
        
        features = features_hs_hd;
        features_train     = features(:,training_indexes);
        features_test      = features(:,test_indexes);
        
        % Clustering of the MS image
        % cluster_data = kmeans(features_ms',max(ground_truth(:)),'Distance','cityblock','MaxIter',500);
        % cluster_image = reshape(cluster_data,[N1 N2]);
        
        
        disp("Nseg actual: "+Nseg);
        
        if size(features_ms,1)>3
            features_ms = hyperPct(features_ms,3);
        end
        
        data_mult = reshape(features_ms',[N1_MS N2_MS 3]);
        
        [segm, Nseg1]  = superpixels(data_mult, Nseg,...
            'Compactness', 20, 'Method',...
            'slic', 'NumIterations', 100);
        features_ms2 = zeros(size(features_ms));
        for nseg = 1:Nseg1
            idx       = find(segm(:) == nseg);
            meanSig = mean(features_ms(:,idx),2);
            features_ms2(:,idx)=repmat(meanSig,1,length(idx));
        end
        
        % Clustering of the superpixels
        cluster_data = kmeans(features_ms2',max(ground_truth(:)),'Distance','cityblock','MaxIter',500);
        cluster_image = reshape(cluster_data,[N1 N2]);
        
        % Stacking
        % features_s = [features; repmat(cluster_data',1,1)];
        features_s = [features; features_ms2];
        features_s_train     = features_s(:,training_indexes);
        features_s_test      = features_s(:,test_indexes);
        
        
        
        % Classification of HS image
        disp('Predicting classes using the SVM-PLY approach...');
        t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
        %         Mdl1        = fitcecoc(features_train',T_classes,'Learners',t);
        %         class_hat1  = predict(Mdl1, features_test');
        
        Mdl2         = fitcecoc(features_s_train',T_classes,'Learners',t);
        class_hat2   = predict(Mdl2, features_s_test');
        
        % Building the classification map
        training_set_image = zeros(size(ground_truth,1), size(ground_truth,2));
        training_set_image(training_indexes) = ground_truth(training_indexes);
        %     SVM_classification_image1 = class_map_image(ground_truth, class_hat1, training_indexes, test_indexes);
        SVM_classification_image2 = class_map_image(ground_truth, class_hat2, training_indexes, test_indexes);
        
        % Majority voting image
        %         majority_image = majority_voting(cluster_image, SVM_classification_image1);
        
        % Computing accuracy
        %     [OA1, AA1, kappa1] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_image1(test_indexes)));
        [OA2, AA2, kappa2,classC] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_image2(test_indexes)));
        
        disp("Overall Accuracy: "+OA2+" Nseg: "+Nseg);
        %         if(OA2>bestOA)
        %             bestOA=OA2;
        %             bestNseg = Nseg;
        %         end
        valsOA(ne) = OA2;
        valsAA(ne) = AA2;
        valsKappa(ne) = kappa2;
        valsClassC(:,ne) = classC';
        valsTime(ne) = toc;
    end
    OAbyNseg(nit) = mean(valsOA);
    AAbyNseg(nit) = mean(valsAA);
    kappabyNseg(nit) = mean(valsKappa);
    classCbyNseg(:,nit) = mean(valsClassC,2);
    timebyNseg(nit) = mean(valsTime); %processing time
    %     save('simu_Nseg','OAbyNseg','nit','Nseg');
end

% Display results
subplot(241)
imshow(mean(I_HS,3),[]);
title('HS image');
subplot(242)
imshow(mean(I_MS,3),[]);
title('MS image');
subplot(243)
imshow(label2rgb(ground_truth));
title('Ground truth');
subplot(244)
imshow(label2rgb(cluster_image));
title('Clustering MS');
subplot(245)
imshow(label2rgb(SVM_classification_image1));
title('Classification HS');
xlabel(['OA = ' num2str(100*OA1,4) ' %']);
subplot(246)
imshow(label2rgb(SVM_classification_image2));
title('Class-Clust Stack');
xlabel(['OA = ' num2str(100*OA2,4) ' %']);
subplot(247)
imshow(label2rgb(majority_image));
title('Majority voting image');


% % %% Including background as a class
% %
% % % training_rate = 0.10;
% % % ground_truth = ground_truth +1;
% % % [training_indexes, test_indexes] = classification_indexes(ground_truth, training_rate);
% % % T_classes = ground_truth(training_indexes);
% % %
% % % features_train     = features(:,training_indexes);
% % % features_test      = features(:,test_indexes);
% %
% % % Classification stage
% % disp('Predicting classes using the SVM-PLY approach...');
% % % t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
% % % Mdl         = fitcecoc(features_train',T_classes,'Learners',t);
% % class_hat   = predict(Mdl1, features');
% % SVM_classification_image = reshape(class_hat,[N1 N2]);
% %
% % % Building the classification map
% % % training_set_image = zeros(size(ground_truth,1), size(ground_truth,2));
% % % training_set_image(training_indexes) = ground_truth(training_indexes);
% % % SVM_classification_image = class_map_image(ground_truth, class_hat, training_indexes, test_indexes);
% %
% % % Majority voting image
% % majority_image = majority_voting(cluster_image, SVM_classification_image2);
% % pr_image = post_regularization(majority_image, 4, 20, 0.00000001);
% %
% % % Computing accuracy
% % [OA, AA, kappa] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_image2(test_indexes)));
% %
% % figure;
% % subplot(231)
% % imshow(label2rgb(ground_truth));
% % title('Ground truth');
% % subplot(232)
% % imshow(label2rgb(cluster_image));
% % title('Clustering with MS');
% % subplot(233)
% % imshow(label2rgb(SVM_classification_image2));
% % title('Classification with HS');
% % % xlabel(['OA = ' num2str(100*OA,4) ' %']);
% % subplot(234)
% % imshow(label2rgb(majority_image));
% % title('Majority voting image');
% % subplot(235)
% % imshow(label2rgb(pr_image));
% % title('Post regularization image');