function [labeled_image] = majority_voting(cluster_image,class_image)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
num_class = max(class_image(:));
num_clust = max(cluster_image(:));
[M,N] = size(cluster_image);
labeled_image = zeros(M,N);
vote = zeros(1,num_clust);

for i = 1:num_class
    indices = find(class_image == i);
    cluster_set = cluster_image(indices);
    for j = 1:num_clust
        vote(j) = sum(cluster_set == j); 
    end
    [~,k] = max(vote);
    labeled_image(indices) = k;
end
end