function [shot_data, shots, num_filters, cca, filter_type] = patterned_shots(I, compression_ratio)

[N1, N2, L] = size(I);
num_filters = floor(L * compression_ratio);
shots       = floor(L * compression_ratio);

[cca, filter_type]  = colored_ca(shots, N1, N2, L, num_filters);
shot_data = zeros(N1, N2, shots);
for i = 1 : shots
    shot_data(:,:,i) = sum(I .* cca{i},3);
end