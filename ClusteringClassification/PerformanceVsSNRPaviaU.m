%%  Spectral Image Classification-Clustering from Compressive Measurements and Data Fusion
%
%   Routine: ClusteringClassification
%
%   Authors:
%   Juan Marcos Ramirez,
%   Carlos Hinojosa
%   Universidad Industrial de Santander, Bucaramanga, Colombia
%   email: juanmarcos26@gmail.com
%
%   Date: May, 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
close all;

addpath(genpath('ReconstructionFunctions'));
addpath(genpath('HSMSFusionToolbox'));

% Loading data
load('../Data/PaviaU.mat');
Io = paviaU(end-255:end,1:256,1:1:96);
clear paviaU;

load('../Data/PaviaU_gt.mat');
ground_truth = paviaU_gt(end-255:end,1:256);
clear paviaU_gt;

L   = size(Io,3);
N1  = size(Io,1);
N2  = size(Io,2);
%dibujar RGB -> imagesc(mat2gray(Io(:,:,[66,53,33])))
% Ground truth correction
for i = 1:N1
    for j = 1:N2
        if (ground_truth(i,j) > 5)
            ground_truth(i,j) = ground_truth(i,j) - 1;
        end
    end
end

% Simulation parameters
SNR                 = 0:5:40;   %additive noise in dB
trials              = 21;
training_rate       = 0.10;
compression_ratio   = 0.25;
Nseg                = 10;

% Building the hyperspectral image (spatial downsampling)
p       = 2;
window  = 5;
sigma   = 1.00;
I_HS    = spatial_blurring(Io, p, window, sigma);
[N1_HS, N2_HS,L_HS] = size(I_HS);


% Building the multispectral image (spectral downsampling)
q = 4;
I_MS = spectral_blurring(Io, q);
[N1_MS, N2_MS,L_MS] = size(I_MS);

for jj = 1:length(SNR)
    for ii = 1:trials
        tic;
        disp(['SNR: ' num2str(SNR(jj)) ' dB. Iteration: ' num2str(ii)]);
        
        num_hs_filters      = floor(L_HS * compression_ratio);
        shots_HS            = floor(L_HS * compression_ratio);
        
        num_ms_filters      = floor(L_MS * compression_ratio);
        shots_MS            = floor(L_MS * compression_ratio);


        [training_indexes, test_indexes] = classification_indexes(ground_truth, training_rate);
        T_classes = ground_truth(training_indexes);
        
        %% Clean Measurements
        % 3D-CASSI shots
        [shot_patt_HS, ~, ~, cca_HS, filter_pattHS] = patterned_shots(I_HS, compression_ratio);
        [shot_patt_MS, ~, ~, cca_MS, filter_pattMS] = patterned_shots(I_MS, compression_ratio);
        
        %% Noisy Measurements
        
        %==> add noise to measurements
        shot_patt_HS_noisy = awgn(reshape(shot_patt_HS,[N1_HS*N2_HS,shots_HS])',...
            SNR(jj),'measured');
        shot_patt_HS_noisy = reshape(shot_patt_HS_noisy',[N1_HS,N2_HS,shots_HS]);
        
        %==> add noise to measurements
        shot_patt_MS_noisy = awgn(reshape(shot_patt_MS,[N1_MS*N2_MS,shots_MS])',...
            SNR(jj),'measured');
        shot_patt_MS_noisy = reshape(shot_patt_MS_noisy',[N1_MS,N2_MS,shots_MS]);
        
        tStart2 = tic;
        % Feature extraction from HS compressive measurements
        features_hs = feature_extraction_Patterned(shot_patt_HS_noisy, size(I_HS),...
            shots_HS, num_hs_filters, filter_pattHS);
        features_hs_hd = interpolate_hs_features(features_hs,[N1_HS N2_HS], p, 'nearest');
        
        features            = features_hs_hd;
        features_train      = features(:,training_indexes);
        features_test       = features(:,test_indexes);
        
        % Feature extraction from MS compressive measurements
        features_ms = feature_extraction_Patterned(shot_patt_MS_noisy, size(I_MS),...
            shots_MS, num_ms_filters, filter_pattMS);
        disp("Nseg actual: "+Nseg);
        
        if size(features_ms,1)>3
            features_ms = hyperPct(features_ms,3);
        end
        
        data_mult = reshape(features_ms',[N1_MS N2_MS 3]);
        
        [segm, Nseg1]  = superpixels(data_mult, Nseg,...
            'Compactness', 20, 'Method',...
            'slic', 'NumIterations', 100);
        features_ms2 = zeros(size(features_ms));
        for nseg = 1:Nseg1
            idx       = find(segm(:) == nseg);
            meanSig = mean(features_ms(:,idx),2);
            features_ms2(:,idx)=repmat(meanSig,1,length(idx));
        end
        
        % Stacking
        features_s          = [features; features_ms2];
        features_s_train    = features_s(:,training_indexes);
        features_s_test     = features_s(:,test_indexes);
        
        % Classification stage
        disp('Predicting classes using the SVM-PLY approach...');
        t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
        
        Mdl2         = fitcecoc(features_s_train',T_classes,'Learners',t);
        class_hat2   = predict(Mdl2, features_s_test');
        
        tElapsed2(ii,jj) = toc(tStart2);
        
        %% Reconstruction + Fusion
        % Recovering hyperspectral image
        tStart4 = tic;
        RH = cassi_reconstruction_admm(shot_patt_HS_noisy, cca_HS);
        RM = cassi_reconstruction_admm(shot_patt_MS_noisy, cca_MS);
        Ir = CNMF_fusion(RH, RM);
        
        features_s          = reshape(Ir,[N1*N2 L])';
        features_s_train    = features_s(:,training_indexes);
        features_s_test     = features_s(:,test_indexes);
        
        % Classification stage
        disp('Predicting classes using the SVM-PLY approach...');
        t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
        
        Mdl4         = fitcecoc(features_s_train',T_classes,'Learners',t);
        class_hat4   = predict(Mdl4, features_s_test');
        
        tElapsed4(ii,jj) = toc(tStart4);
        
        %% Saving classification results
        % Building the classification map
        training_set_image = zeros(size(ground_truth,1), size(ground_truth,2));
        training_set_image(training_indexes) = ground_truth(training_indexes);
        
        SVM_classification_noisy = class_map_image(ground_truth, class_hat2, training_indexes, test_indexes);
        SVM_classification_fused = class_map_image(ground_truth, class_hat4, training_indexes, test_indexes);
        
        [OA2(ii,jj), AA2(ii,jj), kappa2(ii,jj)] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_noisy(test_indexes)));
        [OA4(ii,jj), AA4(ii,jj), kappa4(ii,jj)] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_fused(test_indexes)));
        
        toc;
    end
end

%% Display results
% Overall accuracy
mOA(1,:) = mean(OA2(2:end,:));
mOA(2,:) = mean(OA4(2:end,:));

% Average accuracy
mAA(1,:) = mean(AA2(2:end,:));
mAA(2,:) = mean(AA4(2:end,:));

% Cohen's kappa statistic
mkappa(1,:) = mean(kappa2(2:end,:));
mkappa(2,:) = mean(kappa4(2:end,:));

subplot(131);
plot(SNR, mOA*100,'LineWidth',2);
legend('Compressive-noisy','Rec-CNMF');
axis('tight');
ylabel('OA (%)');
xlabel('SNR [dB]');

subplot(132);
plot(SNR, mAA*100,'LineWidth',2);
legend('Compressive-noisy','Rec-CNMF');
axis('tight');
ylabel('AA (%)');
xlabel('SNR [dB]');

subplot(133);
plot(SNR, mkappa,'LineWidth',2);
legend('Compressive-noisy','Rec-CNMF');
axis('tight');
ylabel('\kappa');
xlabel('SNR [dB]');

data1 = [SNR' 100*mOA'];
save OAvsSNR.dat data1 -ascii;
save OAvsSNR.mat data1;
data2 = [SNR' 100*mAA'];
save AAvsSNR.dat data2 -ascii;
save AAvsSNR.mat data2;
data3 = [SNR' mkappa'];
save KAPPAvsSNR.dat data3 -ascii;
save KAPPAvsSNR.mat data3;