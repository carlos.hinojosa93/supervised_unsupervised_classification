clear all
close all
clc

load('../Data/PaviaU.mat');
Io = paviaU(end-255:end,1:256,1:1:96);
clear paviaU;

load('../Data/PaviaU_gt.mat');
ground_truth = paviaU_gt(end-255:end,1:256);
clear paviaU_gt;

L   = size(Io,3);
N1  = size(Io,1);
N2  = size(Io,2);

[idx1] = find(ground_truth==1);
f1 = zeros(L,1);
for k = 1:length(idx1)
    [i1,j1] = ind2sub([N1,N2],idx1(k));
    f = Io(i1,j1,:);
    f = f(:);
    f1 = f1 + f;
end
f1 = f1/length(idx1);

[idx2] = find(ground_truth==2);
f2 = zeros(L,1);
for k = 1:length(idx2)
    [i1,j1] = ind2sub([N1,N2],idx2(k));
    f = Io(i1,j1,:);
    f = f(:);
    f2 = f2 + f;
end
f2 = f2/length(idx2);

[idx3] = find(ground_truth==3);
f3 = zeros(L,1);
for k = 1:length(idx3)
    [i1,j1] = ind2sub([N1,N2],idx3(k));
    f = Io(i1,j1,:);
    f = f(:);
    f3 = f3 + f;
end
f3 = f3/length(idx3);

[idx4] = find(ground_truth==4);
f4 = zeros(L,1);
for k = 1:length(idx4)
    [i1,j1] = ind2sub([N1,N2],idx4(k));
    f = Io(i1,j1,:);
    f = f(:);
    f4 = f4 + f;
end
f4 = f4/length(idx4);

[idx6] = find(ground_truth==6);
f6 = zeros(L,1);
for k = 1:length(idx6)
    [i1,j1] = ind2sub([N1,N2],idx6(k));
    f = Io(i1,j1,:);
    f = f(:);
    f6 = f6 + f;
end
f6 = f6/length(idx6);

[idx7] = find(ground_truth==7);
f7 = zeros(L,1);
for k = 1:length(idx7)
    [i1,j1] = ind2sub([N1,N2],idx7(k));
    f = Io(i1,j1,:);
    f = f(:);
    f7 = f7 + f;
end
f7 = f7/length(idx7);

[idx8] = find(ground_truth==8);
f8 = zeros(L,1);
for k = 1:length(idx8)
    [i1,j1] = ind2sub([N1,N2],idx8(k));
    f = Io(i1,j1,:);
    f = f(:);
    f8 = f8 + f;
end
f8 = f8/length(idx8);

[idx9] = find(ground_truth==9);
f9 = zeros(L,1);
for k = 1:length(idx9)
    [i1,j1] = ind2sub([N1,N2],idx9(k));
    f = Io(i1,j1,:);
    f = f(:);
    f9 = f9 + f;
end
f9 = f9/length(idx9);

% f1 = Io(106,221,:);
% f1 = f1(:);
% 
% f2 = Io(245,162,:);
% f2 = f2(:);
% 
% f3 = Io(11,38,:);
% f3 = f3(:);
% 
% f4 = Io(214,111,:);
% f4 = f4(:);
% 
% f6 = Io(7,189,:);
% f6 = f6(:);
% 
% f7 = Io(11,149,:);
% f7 = f7(:);
% 
% f8 = Io(203,57,:);
% f8 = f8(:);
% 
% f9 = Io(40,36,:);
% f9 = f9(:);

figure
plot(f1,'LineWidth',2,'Color',[0.81,0.82,0.78]);
hold on
plot(f2,'LineWidth',2,'Color',[26/255,51/255,102/255])
hold on
plot(f3,'LineWidth',2,'Color',[0.4078,0.4941,0.5569])
hold on
plot(f4,'LineWidth',2,'Color',[0,0.502,0])
hold on
plot(f6,'LineWidth',2,'Color',[0.6667,0.2667,0])
hold on
plot(f7,'LineWidth',2,'Color',[0.502,0,0.502])
hold on
plot(f8,'LineWidth',2,'Color',[255/255,179/255,0])
hold on
plot(f9,'LineWidth',2,'Color',[1,1,0])

legend('Asphalt','Meadows','Gravel','Trees','Bare-soil','Bitumen','Self-block Bricks','Shadows');

title('Spectral Curves');
xlabel('Band Number');
ylabel('Reflectance');