function [ img ] = drawResults(grps,M,N)

img(find(grps==1),1)=0.81;
img(find(grps==1),2)=0.82;
img(find(grps==1),3)=0.78;

img(find(grps==2),1)=26/255;
img(find(grps==2),2)=51/255;
img(find(grps==2),3)=102/255;

% img(find(grps==3),1)=0.81;
img(find(grps==3),1)=0.4078;
img(find(grps==3),2)=0.4941;
img(find(grps==3),3)=0.5569;


img(find(grps==4),2)=0.502;

img(find(grps==5),1)=170/255;
img(find(grps==5),2)=68/255;

img(find(grps==6),1)=128/255;
img(find(grps==6),3)=128/255;

img(find(grps==7),1)=255/255;
img(find(grps==7),2)=179/255;

img(find(grps==8),1)=255/255;
img(find(grps==8),2)=255/255;
img(find(grps==8),3)=0;

img(find(grps==9),1)=1;
img(find(grps==9),2)=1;

%image(reshape(img,M,N,3))
img = reshape(img,M,N,3);

end

%pavia university cropped region: ground_truth = paviaU_gt(end-255:end,1:256);
% 1 -> asphalt
% 2 -> meadows
% 3 -> gravel
% 4 -> trees
% 6 -> bare-soil 
% 7 -> bitumen
% 8 -> self-block bricks
% 9 -> shadows