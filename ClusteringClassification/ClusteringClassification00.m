%%  Spectral Image Classification-Clustering from Compressive Measurements and Data Fusion
%
%   Routine: Classification Maps
%
%   Author:
%   Juan Marcos Ramirez,
%   Universidad Industrial de Santander, Bucaramanga, Colombia
%   email: juanmarcos26@gmail.com
%
%   Date: May, 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
close all;

load('pavia3.mat');
Io = paviaU(:,:,1:96);
ground_truth = paviaU_gt;

clear paviaU;
clear paviaU_gt;

L   = size(Io,3);
N1  = size(Io,1);
N2  = size(Io,2);

% Ground truth correction
for i = 1:N1
    for j = 1:N2
        if (ground_truth(i,j) > 2)
            ground_truth(i,j) = ground_truth(i,j) - 1;
        end
    end
end

training_rate = 0.2;
[training_indexes, test_indexes] = classification_indexes(ground_truth, training_rate);
T_classes = ground_truth(training_indexes);

% Building the hyperspectral image (spatial downsampling)
p       = 2;
window  = 3;
sigma   = 1.00;
I_HS    = spatial_blurring(Io, p, window, sigma);

% q = 4;
% I_MS = spectral_blurring(Io, q);
% save Pavia_MS.mat I_MS;

% SSCSI shots
compression_ratio   = 0.25;
[N1_HS, N2_HS,L_HS] = size(I_HS);
num_hs_filters      = floor(L_HS * compression_ratio);
shots_HS            = floor(L_HS * compression_ratio);
[shot_patt_HS, ~, ~, ~, filter_pattHS] = patterned_shots(I_HS, compression_ratio);

% Feature extraction from HS compressive measurements
features_hs = feature_extraction_Patterned(shot_patt_HS, size(I_HS),...
    shots_HS, num_hs_filters, filter_pattHS);
clear shot_patt_HS filter_pattHS;

% Nearest neighbor interpolation
train_indexes_hs    = floor((training_indexes-1)/p) - (N1/p) * floor((training_indexes-1)/(N1)) + (N1/p) * floor((training_indexes-1)/(p*N1));
features_train      = features_hs(:,train_indexes_hs + 1);
test_indexes_hs     = floor((test_indexes-1)/p) - (N1/p) * floor((test_indexes-1)/(N1)) + (N1/p) * floor((test_indexes-1)/(p*N1));
features_test       = features_hs(:,test_indexes_hs + 1);

% Classification stage
disp(['Predicting classes using the SVM-PLY approach...']);
t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
Mdl         = fitcecoc(features_train',T_classes,'Learners',t);
class_hat   = predict(Mdl, features_test');

% Building the classification map
training_set_image = zeros(size(ground_truth,1), size(ground_truth,2));
training_set_image(training_indexes) = ground_truth(training_indexes);
SVM_classification_image = class_map_image(ground_truth, class_hat, training_indexes, test_indexes);

% Computing accuracy
[OA, AA, kappa] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_image(test_indexes)));

load Res.mat;

subplot(141)
imshow(Io(:,:,48),[]);
title('Raw band');
% subplot(142)
% imshow(I_HS(:,:,48),[]);
% title('HS band');
subplot(142)
imshow(label2rgb(ground_truth));
title('Ground truth');
subplot(143)
imshow(label2rgb(SVM_classification_image));
title('Classification with the HS image');
subplot(144)
imshow(Res{2});
title('Clustering with the MS image');