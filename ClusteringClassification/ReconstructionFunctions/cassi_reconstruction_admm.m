function Irec = cassi_reconstruction_admm(shot_data, cca)

[N1, ~, L]      = size(cca{1});
shots           = size(shot_data, 3);

H = [];
for j=1:shots
    ax = [];
    bx = [];
    for i = 1 : L
        vector_code = reshape(cca{j}(:,:,i),[N1*N1,1]);
        a = find(vector_code);
        b = a + N1*N1*(i-1);
        ax = [ax; a];
        bx = [bx; b];
    end
    Ad = sparse(ax,bx,1,N1*N1,N1^2*L);
    H = [H; Ad];
end

B         = wav2_dct1(N1, N1, L);
val       = powr_mthd(H, [N1*N1*L 1], 1e-6, 100, 0);
H         = H./sqrt(val);

y = shot_data(:)/norm(shot_data(:));

lmb_max   = norm(B*(H'*y), inf); % maximo lambda
Irec     = linz_admm(H, B', y, 0.01*lmb_max, [], zeros(N1,N1,L));
Irec     = reshape(Irec, N1, N1, L);