%%  Spectral Image Classification-Clustering from Compressive Measurements and Data Fusion
%
%   Routine: ClusteringClassification
%
%   Author:
%   Juan Marcos Ramirez,
%   Universidad Industrial de Santander, Bucaramanga, Colombia
%   email: juanmarcos26@gmail.com
%
%   Date: May, 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
close all;

addpath(genpath('ReconstructionFunctions'))

% Loading data
load('../Data/PaviaU.mat');
Io = paviaU(end-255:end,1:256,1:1:96);
clear paviaU;

load('../Data/PaviaU_gt.mat');
ground_truth = paviaU_gt(end-255:end,1:256);
clear paviaU_gt;

L   = size(Io,3);
N1  = size(Io,1);
N2  = size(Io,2);

% Ground truth correction
for i = 1:N1
    for j = 1:N2
        if (ground_truth(i,j) > 5)
            ground_truth(i,j) = ground_truth(i,j) - 1;
        end
    end
end

training_rate = 0.10;
[training_indexes, test_indexes] = classification_indexes(ground_truth, training_rate);
T_classes = ground_truth(training_indexes);

% Building the hyperspectral image (spatial downsampling)
p       = 2;
window  = 5;
sigma   = 1.00;
I_HS    = spatial_blurring(Io, p, window, sigma);

q = 8;
I_MS = spectral_blurring(Io, q);

% 3D-CASSI shots
compression_ratio   = 0.25;
[N1_HS, N2_HS,L_HS] = size(I_HS);
num_hs_filters      = floor(L_HS * compression_ratio);
shots_HS            = floor(L_HS * compression_ratio);
[shot_patt_HS, ~, ~, cca_HS, filter_pattHS] = patterned_shots(I_HS, compression_ratio);

[N1_MS, N2_MS,L_MS] = size(I_MS);
num_ms_filters      = floor(L_MS * compression_ratio);
shots_MS            = floor(L_MS * compression_ratio);
[shot_patt_MS, ~, ~, cca_MS, filter_pattMS] = patterned_shots(I_MS, compression_ratio);

% Recovering hyperspectral image
RH = cassi_reconstruction_admm(shot_patt_HS, cca_HS);

% Recovering multispectral image
RM = cassi_reconstruction_admm(shot_patt_MS, cca_MS);


% Feature extraction from HS compressive measurements
features_hs = feature_extraction_Patterned(shot_patt_HS, size(I_HS),...
    shots_HS, num_hs_filters, filter_pattHS);
features_hs_hd = interpolate_hs_features(features_hs,[N1_HS N2_HS], p, 'nearest');
clear shot_patt_HS filter_pattHS;

features_ms = feature_extraction_Patterned(shot_patt_MS, size(I_MS),...
    shots_MS, num_ms_filters, filter_pattMS);
clear shot_patt_HS filter_pattHS;

features = features_hs_hd;
features_train     = features(:,training_indexes);
features_test      = features(:,test_indexes);

% Clustering of the MS image
% cluster_data = kmeans(features_ms',max(ground_truth(:)),'Distance','cityblock','MaxIter',500);
% cluster_image = reshape(cluster_data,[N1 N2]);

Nseg1 = 500;

% segmentation procedure
data_mult = reshape(features_ms',[N1 N2 3]);
[segm, Nseg1]  = superpixels(data_mult, Nseg1,...
                        'Compactness', 20, 'Method',...
                               'slic', 'NumIterations', 100);
features_ms2 = zeros(size(features_ms));

Nseg2 = 1500;

[segm2, Nseg2]  = superpixels(data_mult, Nseg2,...
                        'Compactness', 20, 'Method',...
                               'slic', 'NumIterations', 100);

features_ms3 = zeros(size(features_ms));

Nseg3 = 2500;
[segm3, Nseg3]  = superpixels(data_mult, Nseg3,...
                        'Compactness', 20, 'Method',...
                               'slic', 'NumIterations', 100);

features_ms4 = zeros(size(features_ms));
for nseg = 1:Nseg1
    idx       = find(segm(:) == nseg);
    meanSig = mean(features_ms(:,idx),2);
    features_ms2(:,idx)=repmat(meanSig,1,length(idx));
end 

for nseg = 1:Nseg2
    idx       = find(segm2(:) == nseg);
    meanSig = mean(features_ms(:,idx),2);
    features_ms3(:,idx)=repmat(meanSig,1,length(idx));
end 

for nseg = 1:Nseg3
    idx       = find(segm3(:) == nseg);
    meanSig = mean(features_ms(:,idx),2);
    features_ms4(:,idx)=repmat(meanSig,1,length(idx));
end 

% Clustering of the superpixels
cluster_data = kmeans(features_ms2',max(ground_truth(:)),'Distance','cityblock','MaxIter',500);
cluster_image = reshape(cluster_data,[N1 N2]);

% Stacking
% features_s = [features; repmat(cluster_data',1,1)];
% features_s = [features; features_ms2; features_ms3; features_ms4];
features_s = [features; features_ms3];
features_s_train     = features_s(:,training_indexes);
features_s_test      = features_s(:,test_indexes);

% Classification of HS image
disp('Predicting classes using the SVM-PLY approach...');
t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
Mdl1        = fitcecoc(features_train',T_classes,'Learners',t);
class_hat1  = predict(Mdl1, features_test');

Mdl2         = fitcecoc(features_s_train',T_classes,'Learners',t);
class_hat2   = predict(Mdl2, features_s_test');

% Building the classification map
training_set_image = zeros(size(ground_truth,1), size(ground_truth,2));
training_set_image(training_indexes) = ground_truth(training_indexes);
SVM_classification_image1 = class_map_image(ground_truth, class_hat1, training_indexes, test_indexes);
SVM_classification_image2 = class_map_image(ground_truth, class_hat2, training_indexes, test_indexes);

% Majority voting image
majority_image = majority_voting(cluster_image, SVM_classification_image2);

% Computing accuracy
[OA1, AA1, kappa1] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_image1(test_indexes)));
[OA2, AA2, kappa2] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_image2(test_indexes)));

% Display results
subplot(241)
imshow(mean(I_HS,3),[]);
title('HS image');
subplot(242)
imshow(mean(I_MS,3),[]);
title('MS image');
subplot(243)
imshow(label2rgb(ground_truth));
title('Ground truth');
subplot(244)
imshow(label2rgb(cluster_image));
title('Clustering MS');
subplot(245)
imshow(label2rgb(SVM_classification_image1));
title('Classification HS');
xlabel(['OA = ' num2str(100*OA1,4) ' %']);
subplot(246)
imshow(label2rgb(SVM_classification_image2));
title('Class-Clust Stack');
xlabel(['OA = ' num2str(100*OA2,4) ' %']);
subplot(247)
imshow(label2rgb(majority_image));
title('Majority voting image');


%% Including background as a class

% training_rate = 0.10;
% ground_truth = ground_truth +1;
% [training_indexes, test_indexes] = classification_indexes(ground_truth, training_rate);
% T_classes = ground_truth(training_indexes);
% 
% features_train     = features(:,training_indexes);
% features_test      = features(:,test_indexes);

% Classification stage
disp('Predicting classes using the SVM-PLY approach...');
% t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
% Mdl         = fitcecoc(features_train',T_classes,'Learners',t);
class_hat   = predict(Mdl1, features');
SVM_classification_image = reshape(class_hat,[N1 N2]);

% Building the classification map
% training_set_image = zeros(size(ground_truth,1), size(ground_truth,2));
% training_set_image(training_indexes) = ground_truth(training_indexes);
% SVM_classification_image = class_map_image(ground_truth, class_hat, training_indexes, test_indexes);

% Majority voting image
majority_image = majority_voting(cluster_image, SVM_classification_image);
pr_image = post_regularization(majority_image, 4, 20, 0.00000001);

% Computing accuracy
[OA, AA, kappa] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_image(test_indexes)));

figure;
subplot(231)
imshow(label2rgb(ground_truth));
title('Ground truth');
subplot(232)
imshow(label2rgb(cluster_image));
title('Clustering with MS');
subplot(233)
imshow(label2rgb(SVM_classification_image));
title('Classification with HS');
% xlabel(['OA = ' num2str(100*OA,4) ' %']);
subplot(234)
imshow(label2rgb(majority_image));
title('Majority voting image');
subplot(235)
imshow(label2rgb(pr_image));
title('Post regularization image');