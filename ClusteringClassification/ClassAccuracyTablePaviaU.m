%%  Spectral Image Classification-Clustering from Compressive Measurements and Data Fusion
%
%   Routine: ClusteringClassification
%
%   Authors:
%   Juan Marcos Ramirez,
%   Carlos Hinojosa
%   Universidad Industrial de Santander, Bucaramanga, Colombia
%   email: juanmarcos26@gmail.com
%
%   Date: May, 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
close all;

addpath(genpath('ReconstructionFunctions'));
addpath(genpath('HSMSFusionToolbox'));

% Loading data
load('../Data/PaviaU.mat');
Io = paviaU(end-255:end,1:256,1:1:96);
clear paviaU;

load('../Data/PaviaU_gt.mat');
ground_truth = paviaU_gt(end-255:end,1:256);
clear paviaU_gt;

L   = size(Io,3);
N1  = size(Io,1);
N2  = size(Io,2);
%dibujar RGB -> imagesc(mat2gray(Io(:,:,[66,53,33])))
% Ground truth correction
for i = 1:N1
    for j = 1:N2
        if (ground_truth(i,j) > 5)
            ground_truth(i,j) = ground_truth(i,j) - 1;
        end
    end
end

% Simulation parameters
SNR                 = 25;   %additive noise in dB
trials              = 3;
training_rate       = 0.10;
compression_ratio   = 0.25;
Nseg                = 10;

% Building the hyperspectral image (spatial downsampling)
p       = 2;
window  = 5;
sigma   = 1.00;
I_HS    = spatial_blurring(Io, p, window, sigma);
[N1_HS, N2_HS,L_HS] = size(I_HS);
num_hs_filters      = floor(L_HS * compression_ratio);
shots_HS            = floor(L_HS * compression_ratio);

% Building the multispectral image (spectral downsampling)
q = 8;
I_MS = spectral_blurring(Io, q);
[N1_MS, N2_MS,L_MS] = size(I_MS);
num_ms_filters      = floor(L_MS * compression_ratio);
shots_MS            = floor(L_MS * compression_ratio);

for ii = 1:trials
    tic;
    [training_indexes, test_indexes] = classification_indexes(ground_truth, training_rate);
    T_classes = ground_truth(training_indexes);
    
    %% Clean Measurements
    % 3D-CASSI shots
    [shot_patt_HS, ~, ~, cca_HS, filter_pattHS] = patterned_shots(I_HS, compression_ratio);
    [shot_patt_MS, ~, ~, cca_MS, filter_pattMS] = patterned_shots(I_MS, compression_ratio);
    
    tStart1 = tic;
    % Feature extraction from HS compressive measurements
    features_hs = feature_extraction_Patterned(shot_patt_HS, size(I_HS),...
        shots_HS, num_hs_filters, filter_pattHS);
    features_hs_hd = interpolate_hs_features(features_hs,[N1_HS N2_HS], p, 'nearest');
    
    features            = features_hs_hd;
    features_train      = features(:,training_indexes);
    features_test       = features(:,test_indexes);
    
    % Feature extraction from MS compressive measurements
    features_ms = feature_extraction_Patterned(shot_patt_MS, size(I_MS),...
        shots_MS, num_ms_filters, filter_pattMS);
    disp("Nseg actual: "+Nseg);
    
    if size(features_ms,1)>3
        features_ms = hyperPct(features_ms,3);
    end
    
    data_mult = reshape(features_ms',[N1_MS N2_MS 3]);
    
    [segm, Nseg1]  = superpixels(data_mult, Nseg,...
        'Compactness', 20, 'Method',...
        'slic', 'NumIterations', 100);
    features_ms2 = zeros(size(features_ms));
    for nseg = 1:Nseg1
        idx       = find(segm(:) == nseg);
        meanSig = mean(features_ms(:,idx),2);
        features_ms2(:,idx)=repmat(meanSig,1,length(idx));
    end
    
    % Stacking
    features_s          = [features; features_ms2];
    features_s_train    = features_s(:,training_indexes);
    features_s_test     = features_s(:,test_indexes);

    % Classification stage
    disp('Predicting classes using the SVM-PLY approach...');
    t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
    
    Mdl1         = fitcecoc(features_s_train',T_classes,'Learners',t);
    class_hat1   = predict(Mdl1, features_s_test');
    
    tElapsed1(ii) = toc(tStart1);
    
    
    %% Noisy Measurements

    %==> add noise to measurements
    shot_patt_HS_noisy = awgn(reshape(shot_patt_HS,[N1_HS*N2_HS,shots_HS])',...
        SNR,'measured');
    shot_patt_HS_noisy = reshape(shot_patt_HS_noisy',[N1_HS,N2_HS,shots_HS]);
    
    %==> add noise to measurements
    shot_patt_MS_noisy = awgn(reshape(shot_patt_MS,[N1_MS*N2_MS,shots_MS])',...
        SNR,'measured');
    shot_patt_MS_noisy = reshape(shot_patt_MS_noisy',[N1_MS,N2_MS,shots_MS]);
    
    tStart2 = tic;
    % Feature extraction from HS compressive measurements
    features_hs = feature_extraction_Patterned(shot_patt_HS_noisy, size(I_HS),...
        shots_HS, num_hs_filters, filter_pattHS);
    features_hs_hd = interpolate_hs_features(features_hs,[N1_HS N2_HS], p, 'nearest');
    
    features            = features_hs_hd;
    features_train      = features(:,training_indexes);
    features_test       = features(:,test_indexes);
    
    % Feature extraction from MS compressive measurements
    features_ms = feature_extraction_Patterned(shot_patt_MS_noisy, size(I_MS),...
        shots_MS, num_ms_filters, filter_pattMS);
    disp("Nseg actual: "+Nseg);
    
    if size(features_ms,1)>3
        features_ms = hyperPct(features_ms,3);
    end
    
    data_mult = reshape(features_ms',[N1_MS N2_MS 3]);
    
    [segm, Nseg1]  = superpixels(data_mult, Nseg,...
        'Compactness', 20, 'Method',...
        'slic', 'NumIterations', 100);
    features_ms2 = zeros(size(features_ms));
    for nseg = 1:Nseg1
        idx       = find(segm(:) == nseg);
        meanSig = mean(features_ms(:,idx),2);
        features_ms2(:,idx)=repmat(meanSig,1,length(idx));
    end
    
    % Stacking
    features_s          = [features; features_ms2];
    features_s_train    = features_s(:,training_indexes);
    features_s_test     = features_s(:,test_indexes);

    % Classification stage
    disp('Predicting classes using the SVM-PLY approach...');
    t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
    
    Mdl2         = fitcecoc(features_s_train',T_classes,'Learners',t);
    class_hat2   = predict(Mdl2, features_s_test');
    
    tElapsed2(ii) = toc(tStart2);
    
    %% Raw Image classification
    tStart3 = tic;
    
    features_s          = reshape(Io,[N1*N2 L])';
    features_s_train    = features_s(:,training_indexes);
    features_s_test     = features_s(:,test_indexes);
    
    % Classification stage
    disp('Predicting classes using the SVM-PLY approach...');
    t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
    
    Mdl3         = fitcecoc(features_s_train',T_classes,'Learners',t);
    class_hat3   = predict(Mdl3, features_s_test');
    
    tElapsed3(ii) = toc(tStart3);
    
    %% Reconstruction + Fusion
    % Recovering hyperspectral image
    tStart4 = tic;
    RH = cassi_reconstruction_admm(shot_patt_HS, cca_HS);
    RM = cassi_reconstruction_admm(shot_patt_MS, cca_MS);
    Ir = CNMF_fusion(RH, RM);
    
    features_s          = reshape(Ir,[N1*N2 L])';
    features_s_train    = features_s(:,training_indexes);
    features_s_test     = features_s(:,test_indexes);    

    % Classification stage
    disp('Predicting classes using the SVM-PLY approach...');
    t           = templateSVM('KernelFunction','poly','Standardize',1,'KernelScale','auto');
    
    Mdl4         = fitcecoc(features_s_train',T_classes,'Learners',t);
    class_hat4   = predict(Mdl4, features_s_test');
    
    tElapsed4(ii) = toc(tStart4);    
    
    %% Saving classification results
    % Building the classification map
    training_set_image = zeros(size(ground_truth,1), size(ground_truth,2));
    training_set_image(training_indexes) = ground_truth(training_indexes);
    
    SVM_classification_clean = class_map_image(ground_truth, class_hat1, training_indexes, test_indexes);
    SVM_classification_noisy = class_map_image(ground_truth, class_hat2, training_indexes, test_indexes);
    SVM_classification_image = class_map_image(ground_truth, class_hat3, training_indexes, test_indexes);
    SVM_classification_fused = class_map_image(ground_truth, class_hat4, training_indexes, test_indexes);
    
    [OA1(ii), AA1(ii), kappa1(ii),ClassAcc1(:,ii)] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_clean(test_indexes)));
    [OA2(ii), AA2(ii), kappa2(ii),ClassAcc2(:,ii)] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_noisy(test_indexes)));
    [OA3(ii), AA3(ii), kappa3(ii),ClassAcc3(:,ii)] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_image(test_indexes)));
    [OA4(ii), AA4(ii), kappa4(ii),ClassAcc4(:,ii)] = compute_accuracy(ground_truth(test_indexes), uint8(SVM_classification_fused(test_indexes)));
    
    toc;
end
% Class accuracy
mClassAcc1 = mean(ClassAcc1(:,2:end),2);
mClassAcc2 = mean(ClassAcc2(:,2:end),2);
mClassAcc3 = mean(ClassAcc3(:,2:end),2);
mClassAcc4 = mean(ClassAcc4(:,2:end),2);

stdClassAcc1 = std(ClassAcc1(:,2:end),0,2);
stdClassAcc2 = std(ClassAcc2(:,2:end),0,2);
stdClassAcc3 = std(ClassAcc3(:,2:end),0,2);
stdClassAcc4 = std(ClassAcc4(:,2:end),0,2);

% Overall accuracy
mOA1 = mean(OA1(2:end));
mOA2 = mean(OA2(2:end));
mOA3 = mean(OA3(2:end));
mOA4 = mean(OA4(2:end));

stdOA1 = std(OA1(2:end));
stdOA2 = std(OA2(2:end));
stdOA3 = std(OA3(2:end));
stdOA4 = std(OA4(2:end));

% Average accuracy
mAA1 = mean(AA1(2:end));
mAA2 = mean(AA2(2:end));
mAA3 = mean(AA3(2:end));
mAA4 = mean(AA4(2:end));

stdAA1 = std(AA1(2:end));
stdAA2 = std(AA2(2:end));
stdAA3 = std(AA3(2:end));
stdAA4 = std(AA4(2:end));

% Cohen's kappa statistic
mkappa1 = mean(kappa1(2:end));
mkappa2 = mean(kappa2(2:end));
mkappa3 = mean(kappa3(2:end));
mkappa4 = mean(kappa4(2:end));

stdkappa1 = std(kappa1(2:end));
stdkappa2 = std(kappa2(2:end));
stdkappa3 = std(kappa3(2:end));
stdkappa4 = std(kappa4(2:end));

% Computation time
mtElapsed1 = mean(tElapsed1(2:end));
mtElapsed2 = mean(tElapsed2(2:end));
mtElapsed3 = mean(tElapsed3(2:end));
mtElapsed4 = mean(tElapsed4(2:end));

stdtElapsed1 = std(tElapsed1(2:end));
stdtElapsed2 = std(tElapsed2(2:end));
stdtElapsed3 = std(tElapsed3(2:end));
stdtElapsed4 = std(tElapsed4(2:end));


data1=[mClassAcc1 stdClassAcc1 mClassAcc2 stdClassAcc2 mClassAcc3 stdClassAcc3 mClassAcc4 stdClassAcc4]*100
data2=[mOA1*100 stdOA1*100 mOA2*100 stdOA2*100 mOA3*100 stdOA3*100 mOA4*100 stdOA4*100; ...
    mAA1*100 stdAA1*100 mAA2*100 stdAA2*100 mAA3*100 stdAA3*100 mAA4*100 stdAA4*100;...
    mkappa1 stdkappa1 mkappa2 stdkappa2 mkappa3 stdkappa3 mkappa4 stdkappa4;...
    mtElapsed1 stdtElapsed1 mtElapsed2 stdtElapsed2 mtElapsed3 stdtElapsed3 mtElapsed4 stdtElapsed4]

save ClassAccuracyPavia.dat data1 -ascii;
save PerformancePavia.dat data2 -ascii;