\babel@toc {english}{}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Proposed Method}{12}{0}{2}
\beamer@subsectionintoc {2}{1}{Compressive Measurement Acquisition}{12}{0}{2}
\beamer@subsectionintoc {2}{2}{Feature Extraction}{15}{0}{2}
\beamer@sectionintoc {3}{Simulations and Results}{18}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{25}{0}{4}
