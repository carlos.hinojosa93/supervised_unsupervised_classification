function y=convert3Dto2Dpatchwise_sp(x,labels,num)
[m,n,b]=size(x);
x2D=reshape(x,m*n,b)';
y=[];
for i=0:num-1
    index=find(labels==i);
    class=x2D(:,index);
    y=[y class];
end