% ===============================================================

The code in this package implements the Superpixel-Based Sparse Representation (SSR) method for hyperspectral image super-resolution as described in the following paper:

  

Leyuan Fang, Haijie Zhuo, Shutao Li, 
Super-Resolution of Hyperspectral Image via Superpixel-Based Sparse Representation, Neurocomputing, 2017.

Please cite the paper if you are using this code in your research.

Version:       
1.0 beta version

Contact:       
Leyuan Fang <fangleyuan@gmail.com>

% ===============================================================


Overview

------------
Demo_HS_SuperResolution.m is the demo function for reproducing the results.





Contact

------------

If you have questions, problems with the code, or find a bug, please let us know. Contact Leyuan Fang at fangleyuan@gmail.com
