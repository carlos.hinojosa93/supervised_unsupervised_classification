function [] = superResolution(param)
%% superResolution(paramters)
%   Please cite the following paper, if you use the code:
%   Leyuan Fang, Haijie Zhuo and Shutao Li. "Super-resolution of Hyperspectral Image via Superpixel-Based Sparse Representation." Nuerocomputing.
%   The notations below are according to the paper.
%% ========== Optional parameters ============
%   'spams' - Set it to 1 if SPAMS library*** is installed
%       Default: spams = 0 (i.e. no spams library installed)
%               In the case of no SPAMS, provide the right dictionary for correct results.
%   'L' - Number of atoms selected in each iteration of G-SOMP+
%       Default: L = 30
%   'gamma' - Residual decay parameter used by G-SOMP+
%       Default: gamma = 0.99
%   'k' - Number of dictionary atoms to be used in the learning process
%       Default: k = 300
%   'eta' - modeling error
%       Default: 10e-9
%   'HSI' - name of the image HS image file. The data file should be in the
%   working directory.
%       Default: 'Cuprite Mine'
%
% ***SPAMS (SPArse Modeling Software) is an open source tool that can be easily searched on the internet.
%--------------------------------------------------------------
% Set the defaults for the parameters 
%--------------------------------------------------------------
addpath(genpath('.\data'));    %add the path of test data,and the path where dictionary is stored;
addpath(genpath('.\spams-matlab')); %add the path where online dictionary learning method is stored;
addpath(genpath('.\spams-matlab\build')); %add the path where online dictionary learning method is stored;
addpath(genpath('.\ers_matlab_wrapper_v0.1'));%add the path where superpixel segmentation method is stored(ERS);
spams = 0;
L = 30;
gamma = 0.99;
k = 300;
eta = 10e-9;
sig= input('0 for cupriteMine and 1 for indianPine and 2 for pavia (0/1/2): \n');
switch sig
    case 0
        HSI = 'cupriteMine';    % Image to be tested
    case 1
        HSI = 'indianPine';    % Image to be tested
    case 2
        HSI = 'pavia';          % Image to be tested
    otherwise
        disp('A wrong sig has been inputed, please input again:');
end
path=pwd;
%--------------------------------------------------------------
% Simulated Landsat 7 (multispectral image)
%--------------------------------------------------------------
switch sig
    case 0
        T =[7 15 25 42 113 149]; % the bands selected from cuprite mine to simulate Landsat7
    case 1
        T =[9 17 27 47 128 177]; % the bands selected from indian pine to simulate Landsat7
    case 2
        T =[13 33 58 101];       % the bands selected from pavia center to simulate Landsat7(only the first 4 bands are selected since the wavelength of pavia center can't cover the whole 7 bands)
end

%--------------------------------------------------------------
% Read the parameters, if given
%--------------------------------------------------------------
if isfield(param,'spams')
    if param.spams == 0 || param.spams== 1
        spams = param.spams;
    else
        error('wrong value of the parameter spams')
    end
    if param.spams == 1
        disp('The demo will use the dictionary after learning it')
    end
end
if isfield(param, 'L')
    L = param.L;
end
if isfield(param, 'gamma')
    gamma = param.gamma;
end
if isfield(param, 'k');
    k = param.k;
end
if isfield(param, 'eta')
    eta = param.eta;
end
if isfield(param, 'HSI')
    HSI = param.HSI;
end

%--------------------------------------------------------------
% Read the input image and downsample it
%--------------------------------------------------------------
downsampling_scale = 32;
im_structure = load(HSI);
S = double(im_structure.im);
S(find(S(:)<0))=0;% This is adding an offset to make sure there is no negative data
S=S+mean(S(:)); 
S=S/(max(S(:))+min(S(:))); % normalize the data
[M,N,L] = size(S);
% If the image is not square, make it so.
min_dim = min(M,N);
dim = floor(min_dim/8)*8;
S = S(1:dim, 1:dim, :);
[Y_h] = downsample(S, downsampling_scale); % Downsample the ground turth to simulate low resolution HSI;
Y_h_bar = hyperConvert2D(Y_h);  %Transform the 3D data into 2D for post-processing

%--------------------------------------------------------------
% Simulate high resolution multispectral image 
%--------------------------------------------------------------
S_bar = hyperConvert2D(S); %Transform the 3D data into 2D for post-processing
Y=S(:,:,T); %extract selected bands to simulate Landsat7;
%--------------------------------------------------------------
% Learn the dictionary if SPAMS library is installed
% Otherwise load the learned ditionary for 'cuprite mine'.
% Cupritemine_Dictionry should only be used for the Cuprite mine image.
%--------------------------------------------------------------
if param.spams == 1
   param.K = k;
   param.numThreads = 3;
   param.iter = 600;
   param.mode = 1;
   param.lambda = eta;
   param.posD = 1; 
   Phi = mexTrainDL_Memory(Y_h_bar,param);
   switch HSI
       case 'cupriteMine'
           path_data=strcat(path,'\data\cupriteMine_Dictionary.mat');
           save(path_data,'Phi');
       case 'indianPine'
           path_data=strcat(path,'\data\indianPine_Dictionary.mat');
           save(path_data,'Phi');
       case 'pavia'
           path_data=strcat(path,'\data\pavia_Dictionary.mat');
           save(path_data,'Phi');
   end
else
    switch sig
        case 0
           struc = load('cupriteMine_Dictionary');
           Phi = struc.Phi;
        case 1
           struc = load('indianPine_Dictionary');
           Phi = struc.Phi;
        case 2
           struc = load('pavia_Dictionary');
           Phi = struc.Phi;
    end
end
Phi_tilde = Phi(T,:);

%--------------------------------------------------------------
% Segment the MSI into superpixels.
%--------------------------------------------------------------
total_patches=6000;
Y1=reshape(Y,M*N,length(T));
Y1=normalize(Y1);
B=sparsepca(Y1);
Y1=Y1*B;
Y1=reshape(Y1,M,N);
[labels,bmapOnImg]=suppixel(Y1,total_patches);
%--------------------------------------------------------------
% Process image in superpixels manner
%--------------------------------------------------------------
patch_no = 1;
Sparse_code=[];
Y2D=hyperConvert2D(Y);
S_bar_patchwise=convert3Dto2Dpatchwise_sp(S,labels,total_patches);
tic
for i=0:total_patches-1
    if i==patch_no
        disp(['Processing patch ' int2str(patch_no) ' to ' int2str(patch_no+50)])
        patch_no = patch_no + 50;
    end
    index=find(labels==i);    
    temp=Y2D(:,index);
    A = GSOMP_NN(Phi_tilde, temp, 30, gamma);
    Sparse_code=[Sparse_code A];
end
timer = toc;
disp(['SUPPIXEL processing time....: ' int2str(timer) 'seconds']) 
S_bar_cap = Phi * Sparse_code;
RMSE = sqrt((norm((double(im2uint8(S_bar_patchwise)) - double(im2uint8(S_bar_cap))),2))^2/(M*N*L))
%--------------------------------------------------------------                                                                                                                                                                                                                        
% Display images
%--------------------------------------------------------------
disp('Constructing the hyperspectral image from the 2D patches...')                                                                   
[ estimated3D ] = convert2Dto3Dpatchwise_sp( S_bar_cap,labels,total_patches);
err=double(S)- double(estimated3D);
err=abs(err);
err=im2uint8(err);
figure,imshow(estimated3D(:,:,45));
figure,imagesc(err(:,:,45),[0 25]),colormap('jet'),colorbar;                                                                                                                                                                                                                     
end

function [down_im] = downsample(im, scale)
new_pixel_size = scale;
a = size(im,1)/new_pixel_size;
down_im = zeros(a,a,size(im,3));
m = zeros(a, size(im,1));
s = 1:new_pixel_size:size(im,1);
e = new_pixel_size:new_pixel_size:size(im,1);

for l = 1:size(im,3)
    ima(:,:) = im(:,:,l);
    temp1 = [];
    for i = 1:a
        t = sum(ima(s(i):e(i), :));
        temp1 = [temp1; t];
    end 
    temp2 = [];
    for i = 1:a 
        t = sum(temp1(:,s(i):e(i)),2);
        temp2 = [temp2 t];
    end
    temp2 = temp2./repmat(new_pixel_size^2,size(temp2,1),size(temp2,1));
    down_im(:,:,l) = temp2;
end
    
end

function [Image2D] = hyperConvert2D(Image3D)
if (ndims(Image3D) == 2)
    numBands = 1;
    [h, w] = size(Image3D);
else
    [h, w, numBands] = size(Image3D);
end
Image2D = reshape(Image3D, w*h, numBands).';
end

function [Image3D] = hyperConvert3D(Image2D, h, w, numBands)
[numBands, N] = size(Image2D);
if (1 == N)
    Image3D = reshape(Image2D, h, w);
else
    Image3D = reshape(Image2D.', h, w, numBands); 
end
end


function [ Aa ] = GSOMP_NN( D,Y,L,gamma)
[m, n_p] = size(Y);
[m, k] = size(D);
Dn = normc(D);
R = Y;
S = [];
    while 1
        R0 = R;
        [v, indx] = sort(sum((Dn' * normc(R)),2), 'descend');
        j = indx(1:L,1);
        S = union(S, j');   
        Aa = [];
        aa = zeros(k,1);
        for i =1:n_p        
            a = lsqnonneg(D(:,S), Y(:,i));
            aa(S,1) = a;
            Aa = [Aa aa];
            R(:,i) = Y(:,i)- D(:,S)*a;
        end   
        if (norm(R,2) > gamma*norm(R0,2))     
            break;
        end
    end
end


