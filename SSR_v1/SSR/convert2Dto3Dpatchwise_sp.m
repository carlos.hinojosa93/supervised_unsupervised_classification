function y=convert2Dto3Dpatchwise_sp(x,labels,num)
[r,c]=size(x);
y2d=zeros(r,c);
for i=0:num-1
    index=find(labels==i);
    n=length(index);
    y2d(:,index)=x(:,1:n);
    x(:,1:n)=[];
end
y2d=y2d';
y=reshape(y2d,sqrt(c),sqrt(c),r);
end