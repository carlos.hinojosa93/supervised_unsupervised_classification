%Please cite the following paper, if you use the code:
% Leyuan Fang, Haijie Zhuo and Shutao Li. "Super-resolution of Hyperspectral Image via Superpixel-Based Sparse Representation." Nuerocomputing.


% Running the file will run the demo of the presented approach on the
% 'Cuprite Mine' image. 

%
%-------------------------------------------
% Possible choices 
%-------------------------------------------
% (a)Simply run the demo without SPAMS***(see below) installed. (set
% param.spams = 0)
%       It will read the image in the folder and use the already learned
%       dictionary to generate the results.
% (b)Run the demo with SPAMS already installed. (set param.spams = 1)
%       It will run the complete approach, including the dictionary learning step. 
%-------------------------------------------
% Choose data for experiments
%---------------------------------------------
% Sig=0  Cuprite Mine will be chosen for experiments
% Sig=1  Indian Pine will be chosen for experiments
% Sig=2  Pavia Center will be chosen for experiments
%---------------------------------------------
% Setting the parameters to the default values
%-----------------------------------------------
clear all;
clc;
close all;
% sig=2;
param.spams = 0;        % Set = 1 if SPAMS***(see below)is installed, 0 otherwise
param.L = 20;           % Atoms selected in each iteration of G-SOMP+
param.gamma = 0.99;     % Residual decay parameter
param.k = 300;          % Number of dictionary atoms
param.eta = 10e-9;% Modeling error
% switch sig
%     case 0
%         param.HSI = 'cupriteMine';    % Image to be tested
%     case 1
%         param.HSI = 'indianPine';    % Image to be tested
%     case 2
%         param.HSI = 'pavia';         % Image to be tested
%     otherwise
%         disp('A wrong sig has been inputed, please input again:');
% end
%-----------------------------------------
% Run code
%------------------------------------------
superResolution(param)